Scriptname __OC_MCM extends SKI_ConfigBase  

__OC_Alias Property QST auto
Import OpenContainerRedux

int[]  _ID
float[] _VALUE
string[] _behaviorText;
string[] _behaviorFPText;

int function GetVersion()
	return 1
endFunction

Event OnVersionUpdate(int a_version)
	;
endEvent

Event OnConfigInit()
	ModName = "$OpenContainerRedux"
	_ID  = new int[20]
	_VALUE = new float[20]
	_VALUE[1] = 0.0
	_VALUE[2] = 100.0
	_VALUE[3] = 50.0
	_VALUE[4] = 0.0
	_VALUE[18] = 1.0
	_VALUE[19] = 0.0

	_behaviorText  = new string[2]
	_behaviorText[0] = "$OC_Vanilla"
	_behaviorText[1] = "$OC_PickupMotion"

	_behaviorFPText  = new string[3]
	_behaviorFPText[0] = "$OC_NoMotion"
	_behaviorFPText[1] = "$OC_SneakMotion"
	_behaviorFPText[2] = "$OC_Force3rdPersonInstant"
endEvent

Event OnConfigOpen()
;
endEvent
	
Event OnPageReset(String a_Page)
	bool disableTitle = _VALUE[19] as bool
	If (!disableTitle)
		LoadCustomContent("towawot/OpenContainerRedux.dds")
		utility.WaitMenuMode(2)
		UnloadCustomContent()
	EndIf

	SetCursorFillMode(TOP_TO_BOTTOM)
	AddHeaderOption("")
	_ID[0] = AddToggleOption("$OC_Activate", _VALUE[0])
	_ID[6] = AddToggleOption("$OC_LockedChests", _VALUE[6])
	_ID[4] = AddToggleOption("$OC_PlayerHome", _VALUE[4])
	AddEmptyOption()

	AddEmptyOption()
	AddHeaderOption("")
	int flag_LockOverhaulAutoPick = QST.LockOverhaulChecker() as int
	if (flag_LockOverhaulAutoPick)
		flag_LockOverhaulAutoPick = 0
		_ID[15] = AddTextOption("", "Lock Overhaul - Auto Picking Detected.", flag_LockOverhaulAutoPick)
	else
		flag_LockOverhaulAutoPick = 1
		_ID[15] = AddTextOption("", "Lock Overhaul - Auto Picking Undetected.", flag_LockOverhaulAutoPick)
	endif

	SetCursorPosition(1)
	AddHeaderOption("")
	int flag = FlagSwitch(_VALUE[1])
	_ID[1] = AddTextOption("$OC_GeneralBehavior", _behaviorText[_VALUE[1] as int])
	_ID[5] = AddTextOption("$OC_1stPersonBehavior", _behaviorFPText[_VALUE[5] as int], flag)
	_ID[2] = AddSliderOption("$OC_AdjustDistanceThreshold", _VALUE[2], "$OC_Unit", flag)
	_ID[3] = AddSliderOption("$OC_AdjustgHeadingAngle", _VALUE[3], "$OC_Degrees", flag)

	AddEmptyOption()
	AddHeaderOption("")
	_ID[19] = AddToggleOption("$OC_DisableTitle", _VALUE[19])
	_ID[18] = AddToggleOption("$OC_DisableDebugMessage", _VALUE[18])
endEvent

event OnOptionSelect(int a_option)
	int index = _ID.find(a_option)
	if (index > -1)
		if (index == 1)
			int result = CycleTEXT(_behaviorText, _VALUE[index] as int)
			SetTextOptionValue(a_option, _behaviorText[result])
			_VALUE[index] = result as float
			int flag = FlagSwitch(_VALUE[index])
			SetOptionFlags(_ID[2], flag)
			SetOptionFlags(_ID[3], flag)
			SetOptionFlags(_ID[5], flag)
		elseif (index == 5)
			int result = CycleTEXT(_behaviorFPText, _VALUE[index] as int)
			SetTextOptionValue(a_option, _behaviorFPText[result])
			_VALUE[index] = result as float
		else
			float result = toggleFloat(_VALUE[index])
			SetToggleOptionValue(a_option, (result as bool))
			_VALUE[index] = result
		endif
	endif
endEvent

event OnOptionSliderOpen(int a_option)
	int index = _ID.find(a_option)
	if (index > -1)
		if (index == 2)
			SetSliderDialogStartValue(_VALUE[index])
			SetSliderDialogDefaultValue(100)
			SetSliderDialogRange(50, 150)
			SetSliderDialogInterval(5)
		elseif (index == 3)
			SetSliderDialogStartValue(_VALUE[index])
			SetSliderDialogDefaultValue(50)
			SetSliderDialogRange(20, 360)
			SetSliderDialogInterval(5)
		endif
	endif
	return
endEvent

event OnOptionSliderAccept(int a_option, float a_value)
	int index = _ID.find(a_option)
	if (index > -1)
		string suffix = ""
		if (index == 2)
			suffix = "$OC_Unit"
		elseif (index == 3)
			suffix = "$OC_Degrees"
		endif
		if (suffix != "")
			_VALUE[index] = a_value
			SetSliderOptionValue(a_option, a_value, suffix)
		endif
		return
	endif
endEvent

event OnOptionHighlight(int a_option)
	int index = _ID.find(a_option)
	if (index > -1)
		if (index == 0)
			SetInfoText("$OC_ActivateDesc")
		elseif (index == 1)
			SetInfoText("$OC_GeneralBehaviorDesc")
		elseif (index == 2)
			SetInfoText("$OC_AdjustDistanceThresholdDesc")
		elseif (index == 3)
			SetInfoText("$OC_AdjustgHeadingAngleDesc")
		elseif (index == 4)
			SetInfoText("$OC_PlayerHomeDesc")
		elseif (index == 5)
			SetInfoText("$OC_1stPersonBehaviorDesc")
		elseif (index == 6)
			SetInfoText("$OC_LockedChestsDesc")
		elseif (index == 18)
			SetInfoText("$OC_DisableDebugMessageDesc")
		elseif (index == 19)
			SetInfoText("$OC_DisableTitleDesc")
		endif
	endif
endEvent

Event OnConfigClose()
	Actor playerRef = Game.GetPlayer()
	QST.GeneralBehavior = _VALUE[1]
	QST.AdjustDistanceThreshold = _VALUE[2]
	QST.AdjustHeadingAngle = _VALUE[3]
	QST.AllowPlayerHome = _VALUE[4]
	SetValueDLL(QST.AllowPlayerHome, 4)
	QST.FirstPersonBehavior = _VALUE[5]
	QST.AllowLockedChest = _VALUE[6]
	QST.GlobalAllowLocked.SetValue(QST.AllowLockedChest)
	QST.DisableDebugMessage = _VALUE[18]

	float result = QST.LockOverhaulChecker()
	QST.GlobalLockOverhaulAutoPick.SetValue(result)
	if (!QST.DisableDebugMessage && result)
		Debug.Notification("Auto PickingPerk Detected")
	endif

	Perk thisPerk = Game.GetFormFromFile(0x00d62, "OpenContainerRedux.esp") as Perk
	if (thisPerk)
		if (_VALUE[0])
			bool hasThisPerk = playerRef.HasPerk(thisPerk)
			if (!hasThisPerk)
				playerRef.AddPerk(thisPerk)
			endif
		else
			playerRef.RemovePerk(thisPerk)
		endif
	endif
EndEvent

; utility functionos
float function toggleFloat(float x)
	if (x > 0.0)
		return 0.0
	endif
	return 1.0
endFunction

int Function FlagSwitch(float x)
	int flag = OPTION_FLAG_DISABLED
	if (x > 0.0)
		flag = OPTION_FLAG_NONE
	endif
	return flag
endFunction

int Function CycleTEXT(string[] s, int num)
	int index = num + 1
	if (index >= s.length)
		index = 0
	endif
	return index
endFunction
