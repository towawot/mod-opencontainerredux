Scriptname __OC_Alias extends ReferenceAlias  

Import OpenContainerRedux
float Property GeneralBehavior Auto
float Property AdjustDistanceThreshold Auto
float Property AdjustHeadingAngle Auto
float Property AllowPlayerHome Auto
float Property FirstPersonBehavior Auto
float Property AllowLockedChest Auto
GlobalVariable Property GlobalAllowLocked Auto
float Property DisableDebugMessage Auto
GlobalVariable Property GlobalLockOverhaulAutoPick Auto

event Oninit()
; 	OnPlayerLoadGame()
endEvent

event OnPlayerLoadGame()
	SetValueDLL(AllowPlayerHome, 4)

	float result = LockOverhaulChecker()
	GlobalLockOverhaulAutoPick.SetValue(result)
endEvent

float function LockOverhaulChecker()
	float result = 0.0
	perk thisPerk = none
	if ((Game.GetModByName("Lock Overhaul.esp") < 255) as Bool)
		thisPerk = GetAutoPickingPerk()
		if (thisPerk)
			return 1.0
		else
			return 0.0
		endif
	endif
	return  0.0
endFunction

Perk Function GetAutoPickingPerk()
	int GeneralPerkID = 0x038A0
	int SkyRePerkID = 0x058F1

	Actor playerRef = Game.GetPlayer()
	perk thisPerk = none
	bool hasThisPerk = false
	thisPerk = Game.GetFormFromFile(GeneralPerkID, "Lock Overhaul.esp") as Perk
	if (thisPerk)
		hasThisPerk = playerRef.hasPerk(thisPerk)
		if (hasThisPerk)
			return thisPerk
		else
; 		skyRe Perk check
			thisPerk =Game.GetFormFromFile(SkyRePerkID, "Lock Overhaul.esp") as Perk
			if (thisPerk)
				hasThisPerk = playerRef.hasPerk(thisPerk)
				if (hasThisPerk)
					return thisPerk
				endif
			endif
		endif
	endif
	return none
endFunction
