Scriptname OpenContainerRedux hidden

bool Function HasTimeController(ObjectReference ref) global native
bool Function SetValueDLL(float value, int mode) global native
Function ForceThirdPersonInstant() global native
;untested functions
Function ForceThirdPersonSmooth() global native
Function ForceFirstPersonSmooth() global native
