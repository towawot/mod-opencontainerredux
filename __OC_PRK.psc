;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
;NEXT FRAGMENT INDEX 10
Scriptname __OC_PRK Extends Perk Hidden

;BEGIN FRAGMENT Fragment_0
Function Fragment_0(ObjectReference akTargetRef, Actor akActor)
;BEGIN CODE
	if (simpleLock > 0)
		return
	endif
	simpleLock += 1
	privateRef = akTargetRef

	if(!QST.DisableDebugMessage)
		Debug.Notification("--- Fragment_0 : unlocked/closed ---")
	endif

; 	int lockStatus = GetLockStatus(akTargetRef, akActor)
; 	if (lockStatus == -1)
; 		akTargetRef.Activate(akActor)
; 		simpleLock = 0
; 		privateRef = none
; 		return
; 	endif

	bool switchedPOV = false
	if (QST.GeneralBehavior)
		if (PlayerIsFirstPerson)
			if (QST.FirstPersonBehavior == 2)
				switchedPOV = true
				OpenContainerRedux.ForceThirdPersonInstant()
			endif
		endif
	endif

	bool weaponOut = false
	if (QST.GeneralBehavior)
		if (PlayerIsFirstPerson)
			if (QST.FirstPersonBehavior >= 2)
				weaponOut = ActorSheatheWeapon(akActor)
			endif
		else
			weaponOut = ActorSheatheWeapon(akActor)
		endif
	endif

	string eventName = ""
	if (QST.GeneralBehavior)
		if (PlayerIsFirstPerson)
			if (QST.FirstPersonBehavior >= 1)
				MoveToMarker(akTargetRef, akActor)
			endif
		else
			MoveToMarker(akTargetRef, akActor)
		endif

		eventName = GetAnimEventName(akTargetRef, akActor)
		if (eventName)
			float fDelay = GetAnimDelay(eventName)
			if (eventName == "OC_SneakStart")
				akActor.StartSneaking()
				utility.wait(fDelay)
			else
; 				if (eventName == "IdleLockPick")
; 					Debug.SendAnimationEvent(akActor, "IdleForceDefaultState")
; 				endif
				Debug.SendAnimationEvent(akActor, eventName)
				utility.wait(fDelay)
			endif
		endif
	endif

; 	lockStatus = GetLockStatus(akTargetRef, akActor)
; 	if (lockStatus == 1)
; 		ContainerActivate("LockPick", akTargetRef, akActor)
; 		lockStatus = GetLockStatus(akTargetRef, akActor)
; 		if (lockStatus == 0)
; 			ContainerActivate("Open", akTargetRef, akActor)
; 		endif
; 	else
		ContainerActivate("Open", akTargetRef, akActor)
; 	endif

	if (eventName)
; 		if (!weaponOut)
; 			if (eventName == "IdleLockPick")
; 				utility.wait(0.2)
; 				Debug.SendAnimationEvent(akActor, "IdleForceDefaultState")
; 			endif
; 		endif

		float fDelay = GetAnimDelay(eventName)
		if (eventName == "OC_SneakStart")
			akActor.StartSneaking()
			utility.wait(fDelay)
; 		elseif (weaponOut)
; 			utility.wait(0.5)
			;両手共に魔法の場合
; 			Debug.SendAnimationEvent(akActor, "MagicForceEquip")
			;武器の場合
; 			Debug.SendAnimationEvent(akActor, "WeapOutRightReplaceForceEquip")
		endif
	endif

	if (switchedPOV)
		if (QST.FirstPersonBehavior == 2)
; 			Debug.SendAnimationEvent(akActor, "torchForceEquip");untested
			Debug.SendAnimationEvent(akActor, "IdleForceDefaultState")
			Game.ForceFirstPerson()
		endif
	endif
	
	simpleLock = 0
	privateRef = none
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_4
Function Fragment_4(ObjectReference akTargetRef, Actor akActor)
;BEGIN CODE
	if (simpleLock > 0)
		return
	endif
	simpleLock += 1
	privateRef = akTargetRef

	if(!QST.DisableDebugMessage)
		Debug.Notification("--- Fragment_4 : locked/closed/HasAutoPickPerk ---")
	endif

	int lockStatus = GetLockStatus(akTargetRef, akActor)
	if (lockStatus == -1)
		akTargetRef.Activate(akActor)
		simpleLock = 0
		privateRef = none
		return
	endif

	bool switchedPOV = false
	lockStatus = GetLockStatus(akTargetRef, akActor)
	if (lockStatus > 1)
		if (QST.GeneralBehavior)
			if (PlayerIsFirstPerson)
				if (QST.FirstPersonBehavior == 2)
					switchedPOV = true
					OpenContainerRedux.ForceThirdPersonInstant()
				endif
			endif
		endif
	endif

	bool weaponOut = false
	lockStatus = GetLockStatus(akTargetRef, akActor)
	if (lockStatus > 1)
		if (QST.GeneralBehavior)
			if (PlayerIsFirstPerson)
				if (QST.FirstPersonBehavior >= 2)
					weaponOut = ActorSheatheWeapon(akActor)
				endif
			else
				weaponOut = ActorSheatheWeapon(akActor)
			endif
		endif
	endif

	string eventName = ""
	lockStatus = GetLockStatus(akTargetRef, akActor)
	if (lockStatus > 1)
		if (QST.GeneralBehavior)
			if (PlayerIsFirstPerson)
				if (QST.FirstPersonBehavior >= 1)
					MoveToMarker(akTargetRef, akActor)
				endif
			else
				MoveToMarker(akTargetRef, akActor)
			endif

			eventName = GetAnimEventName(akTargetRef, akActor)
			if (eventName)
				float fDelay = GetAnimDelay(eventName)
				if (eventName == "OC_SneakStart")
					akActor.StartSneaking()
					utility.wait(fDelay)
				else
					if (eventName == "IdleLockPick")
						Debug.SendAnimationEvent(akActor, "IdleForceDefaultState")
					endif
					Debug.SendAnimationEvent(akActor, eventName)
					utility.wait(fDelay)
				endif
			endif
		endif
	endif

	lockStatus = GetLockStatus(akTargetRef, akActor)
	if (lockStatus == 1)
		ContainerActivate("LockPick", akTargetRef, akActor)
		lockStatus = GetLockStatus(akTargetRef, akActor)
		if (lockStatus == 0)
			ContainerActivate("Open", akTargetRef, akActor)
		endif
	endif

	if (eventName)
		if (!weaponOut)
			if (eventName == "IdleLockPick")
				utility.wait(0.2)
				Debug.SendAnimationEvent(akActor, "IdleForceDefaultState")
			endif
		endif

		float fDelay = GetAnimDelay(eventName)
		if (eventName == "OC_SneakStart")
			akActor.StartSneaking()
			utility.wait(fDelay)
		endif
	endif

	if (switchedPOV)
		if (QST.FirstPersonBehavior == 2)
; 			Debug.SendAnimationEvent(akActor, "torchForceEquip");untested
			Debug.SendAnimationEvent(akActor, "IdleForceDefaultState")
			Game.ForceFirstPerson()
		endif
	endif
	
	simpleLock = 0
	privateRef = none
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_6
Function Fragment_6(ObjectReference akTargetRef, Actor akActor)
;BEGIN CODE
	if (simpleLock > 0)
		return
	endif
	simpleLock += 1
	privateRef = akTargetRef

	if(!QST.DisableDebugMessage)
		Debug.Notification("--- Fragment_6 : locked/closed ---")
	endif
	
	int lockStatus = GetLockStatus(akTargetRef, akActor)
	if (lockStatus == -1)
		akTargetRef.Activate(akActor)
		simpleLock = 0
		privateRef = none
		return
	endif

	bool switchedPOV = false
	lockStatus = GetLockStatus(akTargetRef, akActor)
	if (lockStatus > 1)
		if (QST.GeneralBehavior)
			if (PlayerIsFirstPerson)
				if (QST.FirstPersonBehavior == 2)
					switchedPOV = true
					OpenContainerRedux.ForceThirdPersonInstant()
				endif
			endif
		endif
	endif

	bool weaponOut = false
	lockStatus = GetLockStatus(akTargetRef, akActor)
	if (lockStatus > 1)
		if (QST.GeneralBehavior)
			if (PlayerIsFirstPerson)
				if (QST.FirstPersonBehavior >= 2)
					weaponOut = ActorSheatheWeapon(akActor)
				endif
			else
				weaponOut = ActorSheatheWeapon(akActor)
			endif
		endif
	endif

	string eventName = ""
	lockStatus = GetLockStatus(akTargetRef, akActor)
	if (lockStatus > 1)
		if (QST.GeneralBehavior)
			if (PlayerIsFirstPerson)
				if (QST.FirstPersonBehavior >= 1)
					MoveToMarker(akTargetRef, akActor)
				endif
			else
				MoveToMarker(akTargetRef, akActor)
			endif

			eventName = GetAnimEventName(akTargetRef, akActor)
			if (eventName)
				float fDelay = GetAnimDelay(eventName)
				if (eventName == "OC_SneakStart")
					akActor.StartSneaking()
					utility.wait(fDelay)
				else
					if (eventName == "IdleLockPick")
						Debug.SendAnimationEvent(akActor, "IdleForceDefaultState")
					endif
					Debug.SendAnimationEvent(akActor, eventName)
					utility.wait(fDelay)
				endif
			endif
		endif
	endif

	lockStatus = GetLockStatus(akTargetRef, akActor)
	if (lockStatus == 1)
		ContainerActivate("LockPick", akTargetRef, akActor)
		lockStatus = GetLockStatus(akTargetRef, akActor)
		if (lockStatus == 0)
			ContainerActivate("Open", akTargetRef, akActor)
		endif
	endif

	if (eventName)
		if (!weaponOut)
			if (eventName == "IdleLockPick")
				utility.wait(0.2)
				Debug.SendAnimationEvent(akActor, "IdleForceDefaultState")
			endif
		endif

		float fDelay = GetAnimDelay(eventName)
		if (eventName == "OC_SneakStart")
			akActor.StartSneaking()
			utility.wait(fDelay)
		endif
	endif

	if (switchedPOV)
		if (QST.FirstPersonBehavior == 2)
; 			Debug.SendAnimationEvent(akActor, "torchForceEquip");untested
			Debug.SendAnimationEvent(akActor, "IdleForceDefaultState")
			Game.ForceFirstPerson()
		endif
	endif
	
	simpleLock = 0
	privateRef = none
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_2
Function Fragment_2(ObjectReference akTargetRef, Actor akActor)
;BEGIN CODE
	if (simpleLock > 0)
		return
	endif
	simpleLock += 1
	privateRef = akTargetRef

	if(!QST.DisableDebugMessage)
		Debug.trace("--- Fragment_2 : opened ---")
	endif
	
	bool switchedPOV = false
	if (QST.GeneralBehavior)
		if (PlayerIsFirstPerson)
			if (QST.FirstPersonBehavior == 2)
				switchedPOV = true
				OpenContainerRedux.ForceThirdPersonInstant()
			endif
		endif
	endif
	
	bool weaponOut = false
	if (QST.GeneralBehavior)
		if (PlayerIsFirstPerson)
			if (QST.FirstPersonBehavior >= 2)
				weaponOut = ActorSheatheWeapon(akActor)
			endif
		else
			weaponOut = ActorSheatheWeapon(akActor)
		endif
	endif

	string eventName = ""
	if (QST.GeneralBehavior)
		if (PlayerIsFirstPerson)
			if (QST.FirstPersonBehavior >= 1)
				MoveToMarker(akTargetRef, akActor)
			endif
		else
			MoveToMarker(akTargetRef, akActor)
		endif
		
		eventName = GetAnimEventName(akTargetRef, akActor)
		if (eventName)
			float fDelay = GetAnimDelay(eventName)
			if (eventName == "OC_SneakStart")
				akActor.StartSneaking()
				utility.wait(fDelay)
			else
				Debug.SendAnimationEvent(akActor, eventName)
				utility.wait(fDelay)
			endif
		endif
	endif
	
	ContainerActivate("Close", akTargetRef, akActor)
	utility.wait(0.1)

	if (eventName)
		float fDelay = GetAnimDelay(eventName)
		if (eventName == "OC_SneakStart")
			akActor.StartSneaking()
			utility.wait(fDelay)
; 		elseif (weaponOut)
; 			utility.wait(0.5)
			;両手共に魔法の場合
; 			Debug.SendAnimationEvent(akActor, "MagicForceEquip")
			;武器の場合
; 			Debug.SendAnimationEvent(akActor, "WeapOutRightReplaceForceEquip")
		endif
	endif

	if (switchedPOV)
		if (QST.FirstPersonBehavior == 2)
; 			Debug.SendAnimationEvent(akActor, "torchForceEquip"); untested
			Debug.SendAnimationEvent(akActor, "IdleForceDefaultState")
			Game.ForceFirstPerson()
		endif
	endif
	
	simpleLock = 0
	privateRef = none
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

bool Function ActorSheatheWeapon(Actor akActor)
	if (!akActor)
		return false
	endif
	
	bool result = akActor.IsWeaponDrawn()
	if (result)
		int count = 10
		while (result && count)
			akActor.SheatheWeapon()
			utility.wait(1.0)
			result = akActor.IsWeaponDrawn()
			count -= 1
		endWhile
	endif
	return result
endFunction

float Function GetRequireMove(ObjectReference akTargetRef, Actor akActor)
	if (!akTargetRef || !akActor)
		return 0.0
	endif
	
	float dist = akTargetRef.GetDistance(akActor)
	if (dist > QST.AdjustDistanceThreshold)
		bool locked = akTargetRef.IsLocked()
		if (locked)
			return 0.1
		else
			return (dist / 300)
		endif
	else
		float headingAngle = akActor.getHeadingAngle(akTargetRef)
		float angle1 = QST.AdjustHeadingAngle / 2
		float angle2 = angle1 * -1
		if (!(headingAngle < angle1 && headingAngle > angle2))
			return 0.1
		endif
	endif
	return 0.0
endFunction

Actor function SetMarkerRef(ObjectReference akTargetRef, Actor akActor)
	if (!akTargetRef || !akActor)
		return none
	endif
	Actor markerRef = Game.GetFormFromFile(0x02854, "OpenContainerRedux.esp") as Actor
	if (markerRef)
		markerRef.MoveTo(akActor, abMatchRotation = false)
		markerRef.SetAlpha(0.0, false)
		bool is3DLoad = markerRef.Is3dLoaded()
		if (is3DLoad)
			markerRef.SetMotionType(markerRef.Motion_Keyframed)
		endif
		markerRef.EnableAI(false)
		markerRef.MoveTo(akTargetRef, abMatchRotation = false)
	endif
	return markerRef
endFunction

Function MoveToRef(Actor akTargetRef, Actor akActor, float _fTime)
	if (!akTargetRef || !akActor || _fTime <= 0.0)
		return
	endif
		bool isPlayer = (akActor == Game.GetPlayer())
	if (isPlayer)
		Game.SetPlayerAIDriven(true)
	endif
; 	Game.SetCameraTarget(akTargetRef)
	akActor.KeepOffsetFromActor(akTargetRef, 0.0, 0.0, 20.0, afFollowRadius = 10.0)
	utility.wait(_fTime)
	akActor.ClearKeepOffsetFromActor()
; 	Game.SetCameraTarget(akActor)
	if (isPlayer)
		Game.SetPlayerAIDriven(false)
	endif
endFunction

Function MoveToMarker(ObjectReference akTargetRef, Actor akActor)
	if (!akTargetRef || !akActor)
		return
	endif
	float fTime = 0.0
	fTime = GetRequireMove(akTargetRef, akActor)
	if (fTime > 0.0)
		Actor markerRef = SetMarkerRef(akTargetRef, akActor)
		if (markerRef)
			MoveToRef(markerRef, akActor, fTime)
			markerRef.MoveToMyEditorLocation()
		endif
	endif
endFunction

int Function GetLockStatus(ObjectReference akTargetRef, Actor akActor)
{0:unlocked  1:locked  2:has Key  -1:can't open}
	if (!akTargetRef || !akActor)
		return 0
	endif
	bool locked = akTargetRef.IsLocked()
	if (locked)
		bool requireKey = (akTargetRef.GetLockLevel() == 255) as bool
		if (requireKey)
			Key chestKey = akTargetRef.GetKey()
			bool hasKey = akActor.GetItemCount(chestKey) as bool
			if (!hasKey)
				return -1
			else
				return 2
			endif
		else
			form lockpickForm = Game.GetForm(0xA)
			bool hasLockPick = akActor.GetItemCount(lockpickForm) as bool
			if (!hasLockPick)
				return -1
			else
				return 1
			endif
		endif
	endif
	return 0
endFunction

string Function GetAnimEventName(ObjectReference akTargetRef, Actor akActor)
	if (!akTargetRef || !akActor)
		return none
	endif
	float diff = akTargetRef.Z - akActor.Z
	int lockStatus = GetLockStatus(akTargetRef, akActor)
	if (PlayerIsFirstPerson)
		if (diff > 41.0)
			return none
		endif
		if (QST.FirstPersonBehavior == 1)
			bool isSneak = akActor.IsSneaking()
			if (!isSneak)
				return "OC_SneakStart"
			endif
		endif
		return none
	elseif (lockStatus == 1)
		return "IdleLockPick"
	elseif (lockStatus == 2)
		if (diff > 41.0)
			return "IdleActivatePickUp"
		else
			return "IdleActivatePickUpLow"
		endif
	else
		if (diff > 41.0)
			return "IdleActivatePickUp"
		else
			return "IdleActivatePickUpLow"
		endif
	endif
	return none
endFunction

float Function GetAnimDelay(string _eventName)
	if (_eventName == "IdleLockPick")
		return 4.0
	elseif (_eventName == "IdleActivatePickUpLow")
		return 0.6
	elseif (_eventName == "IdleActivatePickUp")
		return 0.6
	elseif (_eventName == "OC_SneakStart")
		return 0.3
	endif
	return 0.0
endFunction

function ContainerActivate(string _openClose, ObjectReference akTargetRef, Actor akActor)
	Form baseForm = akTargetRef.GetBaseObject()
	if (_openClose == "LockPick")
		ObjectReference dummyTarget = akTargetRef.PlaceAtMe(baseForm, abInitiallyDisabled = true)
		float targetScale = akTargetRef.GetScale()
		dummyTarget.SetScale(targetScale)
		dummyTarget.enableNoWait()
		utility.wait(0.1)

		akTargetRef.Activate(akActor)
		akTargetRef.disable()
		utility.wait(0.05)
		akTargetRef.enable()
		utility.wait(0.1)
		dummyTarget.disable()
		dummyTarget.delete()
	elseif (_openClose == "Open")
		ObjectReference dummyTarget = akTargetRef.PlaceAtMe(baseForm, abInitiallyDisabled = true)
		dummyTarget.SetPosition(akTargetRef.X, akTargetRef.Y, akTargetRef.Z + 10000)
		dummyTarget.enableNoWait()
		utility.wait(0.1)
		
		bool dummyOpen = dummyTarget.PlayGamebryoAnimation("Open", true)
		float targetScale = akTargetRef.GetScale()
		dummyTarget.SetScale(targetScale)

		akTargetRef.AddItem(flag)
		bool isOpen = akTargetRef.PlayGamebryoAnimation("Open", True)
		utility.wait(0.5)

		dummyTarget.DisableNoWait()
		dummyTarget.SetPosition(akTargetRef.X, akTargetRef.Y, akTargetRef.Z)
		dummyTarget.enableNoWait()

		utility.wait(0.1)

		akTargetRef.disable()
		akTargetRef.Activate(akActor)
		utility.wait(0.05)
		akTargetRef.enable()
		utility.wait(0.2)
		dummyTarget.disable()
		dummyTarget.delete()
	elseif (_openClose == "Close")
		bool isClose = akTargetRef.PlayGamebryoAnimation("Close", True)
		akTargetRef.RemoveItem(flag)
	endif
endFunction

__OC_Alias Property QST auto
MiscObject Property flag Auto
ObjectReference privateRef
int simpleLock

Bool property PlayerIsFirstPerson hidden
	bool Function Get()
		Return (Game.GetCameraState() == 0)
	EndFunction
EndProperty

